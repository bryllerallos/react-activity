import React , { Component } from 'react';
import {Table} from 'reactstrap';

class ForexTable extends Component{
    
    state = {}
	render(){
		const rates = Object.entries(this.props.rates)
		return(

			<Table>
                <thead>
                    <tr>
                        <th>Currency</th>
                        <th>Rate</th>
                    </tr>
                </thead>
                <tbody>
                    {rates.map((rate, index)=>{
                        return(
                            <tr key={index}>
                                <td>rate[0]</td>
                                <td>rate[1]</td>
                            </tr>
                        );

                    })}
                </tbody>
            </Table>
        );
	}
}

export default ForexTable;
