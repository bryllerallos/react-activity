import React, {Component} from 'react';
import ForexApp from '../ForexApp';
import ForexDropdown from './ForexDropdown';
import ForexInput from './ForexInput';
import { Button } from 'reactstrap';
import ForexTable from './ForexTable';

class Forex extends Component {
    
    state = {
        amount: 0,
        baseCurrency: "",
        targetCurrency: "",
        convertedAmount: 0,
        rates: {}
    }

    handleAmount = e => {
        this.setState({
            amount: e.target.value
        });
    }

    handleBaseCurrency = currency => {
        this.setState({
            baseCurrency: currency
        });
        // const code = this.state.code;
        // this.setState({code: currency.code});
        
        // fetch('https://api.exchangeratesapi.io/latest?base='+code)
        //     .then( res => res.json())
        //     .then( res => {
   
        //         console.log(res.rates)
        // //  this.setState({ rates:res.rates });
        //     });   
    }

    handleTargetCurrency = currency => {
        this.setState({
            targetCurrency: currency
        });
    }
    


    handleConvert = () => {

        if(this.state.targetCurrency===""|| this.state.baseCurrency ===""){
            alert('Please select Currency');
        }else{
            if(this.state.amount <=0){
                alert('Invalid amount')
            }else{

            }
            const code = this.state.baseCurrency.code; 
        

            fetch('https://api.exchangeratesapi.io/latest?base=' + code)
            .then( res => res.json())
            .then( res => {
                console.log(res)
                const targetCode = this.state.targetCurrency.code;
    
                const rate = res.rates[targetCode];
    
                // console.log(rate);
                this.setState({ convertedAmount: this.state.amount * rate })
            });
        }
 
    }
    
    render(){

       
        return(
            <div className='bg-info'
                style={{width: "70%"}}    
        
            >
                <h1 className='text-center my-5'>Forex Calculator</h1>
                <h2 className='text-center'>{this.state.baseError}</h2>
                <div
                    className='d-flex justify-content-around'
                    style={{margin:'0 200px'}}
                >
                    <ForexDropdown
                        label = { 'Base Currency' }
                        onClick = {this.handleBaseCurrency}
                        currency = {this.state.baseCurrency}
                     />
                    
                     <ForexDropdown
                        label = { 'Target Currency' }
                        onClick = {this.handleTargetCurrency}
                        currency = {this.state.targetCurrency}
                     />
                </div>
                <div
                 className='d-flex justify-content-around align-items-center'
                >
                     <ForexInput
                    label = { 'Amount'}
                    placeholder = {'Amount to Convert'}
                    onChange = {this.handleAmount}
                    />
                    <Button
                        style={{borderRadius: "40px", height: "100%", width:"30%", color: "dark", fontWeight: "700"}}
                        color='success'
                        onClick = {this.handleConvert}
                    >
                    Convert
                    </Button>
                </div>
                <div>
                    <h1 className='text-center'>{this.state.convertedAmount}{this.state.targetCurrency.code}</h1>
                </div>
                    <div style={{maxHeight: "40vh", overflowY: "auto"}}
                    ><ForexTable 
                        rates = {this.state.rates}
                    /></div>
            </div>
         
        )
    }
}

export default Forex;


