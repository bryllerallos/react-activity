//creating class based component
import React, {Component} from 'react';
//import box component
import Box from './components/Box';
//import buttons component
import Buttons from './components/Buttons';


class App extends Component {

  state = {
    count: 0
  }
//step 1 : create the function

  handleAdd = () => {
    this.setState({
      count : this.state.count + 1
      //step 5 : setup the set state function  based on the role of the button
    });
  }

  handleMinus = () => {
    this.setState({
      count : this.state.count - 1
    });
  }

  handleReset = () => {
    this.setState({
      count : this.state.count = 0
    });
  }

  handleMultiply = () => {
    this.setState({
      count : this.state.count * 2
    });
  }

  handleDivide = () => {
    this.setState({
      count: this.state.count / 2
    });
  }

  render() {
    return(
      <React.Fragment>
          <Box
            count = {this.state.count}
           />
          <Buttons
            handleAdd = {this.handleAdd}
            handleMinus = {this.handleMinus}
            handleReset = {this.handleReset}
            handleMultiply = {this.handleMultiply}
            handleDivide = {this.handleDivide}
            

            // step 2 : pass the function as a property to buttons component
           />
           
      </React.Fragment>
     
      
      
    )
  }
}

export default App;